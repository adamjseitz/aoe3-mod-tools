# Scripts

Various AoE3-related scripts live here.

## DBID Cleanup

Designed for the `techtree[x/y].xml` or `proto[x/y].xml`, it cleans up
duplicate and missing DBID numbers. With techs, this ensures ESO
compatibility for HC Shipments, but keeping it clean in both files is
probably wise.

```
usage: python dbid_fix.py [-h] [-v] [--output OUTPUT] input

Resolve DBID conflicts.

positional arguments:
  input            Input filename

optional arguments:
  -h, --help       show this help message and exit
  -v, --verbose    Display more messages
  --output OUTPUT  Output filename
```
